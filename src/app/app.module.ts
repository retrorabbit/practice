import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyDsrRtzOLKpBh6zjurWLSAvYlFck1ap3sU',
  authDomain: 'practice-a507c.firebaseapp.com',
  databaseURL: 'https://practice-a507c.firebaseio.com',
  projectId: 'practice-a507c',
  storageBucket: '',
  messagingSenderId: '470903341859'
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }